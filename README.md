## Generate random english words

## Examples
```
[dependencies]
rand_word = "*"
```
```rust
let words_0 = rand_word::new(20); // generate 20 random words
println!("{:?}", words_0);
```


